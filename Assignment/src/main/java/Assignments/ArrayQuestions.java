package Assignments;

import java.util.Scanner;

public class ArrayQuestions {

	public static void main(String[] args) {
		maximum();
		average();
	}
	
	public static void maximum() {
		Scanner a = new Scanner(System.in); 
		int n = a.nextInt();
		int arr[] = new int[n];
		for(int i=0;i<n;i++) {
			arr[i]= a.nextInt();
		}
		int max = 0;
		for(int i=0;i<arr.length;i++) {
			if(arr[i]>max) {
				max = arr[i];
			}
		}
		System.out.println(max);

	}
	
	public static void average() {
		Scanner a = new Scanner(System.in); 
		int n = a.nextInt();
		int arr[] = new int[n];
		int sum =0;
		for(int i=0;i<n;i++) {
			arr[i]= a.nextInt();
			sum+=arr[i];
		}
		int avg = sum/n;
		
		System.out.println(avg);

	}

}

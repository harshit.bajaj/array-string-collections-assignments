package Assignments;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class CollectionQuestions {

	public static void main(String[] args) {
		removeNamesContainingCharacterHashSet();
		System.out.println("-----------------");
        fruitsWithLengthEvenAndMoreThanFiveList();
	}
	
	public static void fruitsWithLengthEvenAndMoreThanFiveList() {
		List<String> list = new ArrayList<String>();
		list.add("Kiwi");
		list.add("Apple");
		list.add("Banana");
		list.add("Blueberry");

		System.out.println("Fruits with length more than 5 and even: ");
		for(int i=0;i<list.size();i++) {
			if(list.get(i).length()>5 && list.get(i).length()%2==0) {
				System.out.println(list.get(i));
			}
		}

	}
	
	public static void removeNamesContainingCharacterHashSet() {
		Set<String> nameSet = new HashSet<String>();
		nameSet.add("Ross");
		nameSet.add("Sham");
		nameSet.add("Vicky");
		nameSet.add("Rhea");
		System.out.println("Names containing 'a' or 'e': ");

		Iterator<String> nameItr = nameSet.iterator();
		while (nameItr.hasNext()) {
		   String target = nameItr.next(); 
		   if(target.contains("a")|| target.contains("e")) {
			   System.out.println(target);
			   nameItr.remove();
			}
		}
		System.out.println("Modified Set: "+nameSet);

	}

}

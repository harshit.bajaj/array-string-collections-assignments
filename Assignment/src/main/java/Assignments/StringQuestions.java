package Assignments;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

public class StringQuestions {

	public static void main(String[] args) {
	 palindromeString();
	 uniqueAndDuplicates();
	}
	
	public static void palindromeString() {
		Scanner s = new Scanner(System.in); 
		String strng = s.nextLine();
		StringBuffer sb = new StringBuffer(strng);
		if(strng.equals(sb.reverse().toString())){
			System.out.println("string is palindrome");
		}
		else
			System.out.println("string is not palindrome");

	}

	private static void uniqueAndDuplicates() {
		Scanner s = new Scanner(System.in); 
		String strng = s.next();
		HashMap<Character, Boolean> map = new HashMap<Character, Boolean>();
		HashSet<Character> uniqueSet = new HashSet<Character>();
		HashSet<Character> duplicteSet = new HashSet<Character>();
		for (char c : strng.toCharArray()) {
			if(!map.containsKey(c)) {
				map.put(c, true);
			}
			else {
				map.put(c, false);
			}
		}

		for(int i=0;i<strng.toCharArray().length;i++) {
			if(map.get(strng.charAt(i)).equals(false))
				duplicteSet.add(strng.charAt(i));
			else
				uniqueSet.add(strng.charAt(i));
		}

		System.out.println("Unique characters: " + uniqueSet);
		System.out.println("Duplicate characters: " + duplicteSet);

	}

}
